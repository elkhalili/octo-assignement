package ma.octo.assignement.service;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Slf4j
@Service
@Transactional

public class AuditService {

    @Autowired
    private AuditRepository auditRepository;

    public void auditVirement(String message) {

        log.info("Audit de l'événement: {}", EventType.VIREMENT);

        Audit audit = new Audit();
        audit.setEventType(EventType.VIREMENT);
        audit.setMessage(message);
        auditRepository.save(audit);
    }

    public void auditVersement(String message) {

        log.info("Audit de l'événement: {}", EventType.VERSEMENT);

        Audit audit = new Audit();
        audit.setEventType(EventType.VERSEMENT);
        audit.setMessage(message);
        auditRepository.save(audit);
    }
}
