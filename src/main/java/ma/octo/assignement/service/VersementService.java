package ma.octo.assignement.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@Slf4j
@RequiredArgsConstructor
@Service
public class VersementService {

    final private VersementRepository versementRepository;
    final private CompteRepository compteRepository;
    final private AuditService auditService;

    public List<Versement> loadAll() {
        return versementRepository.findAll();
    }


    public void createTransaction(VersementDto versementDto)
            throws CompteNonExistantException {

        Compte compteBeneficiaire = compteRepository.findByRib(versementDto.getRib());

        if (Objects.isNull(compteBeneficiaire)) {
            log.error("CompteB Non existant");
            throw new CompteNonExistantException("CompteB Non existant");
        }


        compteBeneficiaire.setSolde(new BigDecimal(compteBeneficiaire.getSolde().intValue() + versementDto.getMontantVersement().intValue()));
        compteRepository.save(compteBeneficiaire);

        Versement versement = new Versement();
        versement.setNom_prenom_emetteur(versementDto.getNom_prenom_emetteur());
        versement.setDateExecution(versementDto.getDate());
        versement.setCompteBeneficiaire(compteBeneficiaire);
        versement.setMontantVersement(versementDto.getMontantVersement());
        versement.setMotifVersement(versementDto.getMotif());

        versementRepository.save(versement);

        auditService.auditVersement("Versement depuis "+ versementDto.getNom_prenom_emetteur() +
                " vers " + versementDto.getRib() + " d'un montant de " + versementDto.getMontantVersement()
                .toString());
    }
}
