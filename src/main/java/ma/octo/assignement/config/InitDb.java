package ma.octo.assignement.config;
import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VirementRepository;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.Date;

@Configuration
@RequiredArgsConstructor

public class InitDb {

    final private CompteRepository compteRepository;

    final private UtilisateurRepository utilisateurRepository;

    final private VirementRepository virementRepository;

    @PostConstruct
    public void run() {

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("last1");
        utilisateur1.setFirstname("first1");
        utilisateur1.setGender("Male");

        utilisateurRepository.save(utilisateur1);

        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("user2");
        utilisateur2.setLastname("last2");
        utilisateur2.setFirstname("first2");
        utilisateur2.setGender("Female");

        utilisateurRepository.save(utilisateur2);

        Compte compte1 = new Compte();
        compte1.setNrCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);

        compteRepository.save(compte1);

        Compte compte2 = new Compte();
        compte2.setNrCompte("010000B025001000");
        compte2.setRib("RIB2");
        compte2.setSolde(BigDecimal.valueOf(1400));
        compte2.setUtilisateur(utilisateur2);

        compteRepository.save(compte2);

        Virement v = new Virement();
        v.setMontantVirement(BigDecimal.valueOf(10));
        v.setCompteBeneficiaire(compte2);
        v.setCompteEmetteur(compte1);
        v.setDateExecution(new Date());
        v.setMotifVirement("Assignment 2021");

        virementRepository.save(v);
    }
}