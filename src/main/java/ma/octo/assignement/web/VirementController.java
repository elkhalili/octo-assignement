package ma.octo.assignement.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.service.VirementService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/virements")

class VirementController {

    final private VirementService virementService;

    @GetMapping
    List<Virement> loadAll() {
        return virementService.loadAll();
    }

    @PostMapping("/executer")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@Valid @RequestBody VirementDto virementDto)
            throws CompteNonExistantException,
            SoldeDisponibleInsuffisantException {
        virementService.createTransaction(virementDto);
    }

}
