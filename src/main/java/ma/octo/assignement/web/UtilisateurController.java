package ma.octo.assignement.web;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/utilisateurs")

public class UtilisateurController {

    final private UtilisateurRepository utilisateurRepository;

    @GetMapping
    List<Utilisateur> loadAllUtilisateur() {
        return utilisateurRepository.findAll();
    }

}
