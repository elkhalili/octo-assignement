package ma.octo.assignement.web;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.AuditRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/audits")
public class AuditController {

    final private AuditRepository auditRepository;

    @GetMapping
    List<Audit> loadAllAudits() {
        return auditRepository.findAll();
    }

}
