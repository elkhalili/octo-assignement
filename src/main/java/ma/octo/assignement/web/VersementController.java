package ma.octo.assignement.web;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.service.VersementService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/versements")

class VersementController {

        final private VersementService versementService;

        @GetMapping
        List<Versement> loadAll() {
            return versementService.loadAll();
        }

        @PostMapping("/executer")
        @ResponseStatus(HttpStatus.CREATED)
        public void createTransaction(@Valid @RequestBody VersementDto versementDto)
                throws CompteNonExistantException {
            versementService.createTransaction(versementDto);
        }

}
