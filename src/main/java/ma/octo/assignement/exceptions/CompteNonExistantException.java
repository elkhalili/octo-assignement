package ma.octo.assignement.exceptions;

import lombok.extern.slf4j.Slf4j;

public class CompteNonExistantException extends Exception {

  private static final long serialVersionUID = 1L;

  public CompteNonExistantException() {
  }

  public CompteNonExistantException(String message) {
    super(message);}
}
