package ma.octo.assignement.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class VirementDto {

  @NotBlank
  private String nrCompteEmetteur;

  @NotBlank
  private String nrCompteBeneficiaire;

  @NotBlank(message = "Motif vide")
  private String motif;

  @Min(value = 10, message = "Montant minimal de virement non atteint")
  @Max(value = 10_000, message = "Montant maximal de virement dépassé")
  @NotNull(message = "Montant vide")
  private BigDecimal montantVirement;

  private Date date;

}
