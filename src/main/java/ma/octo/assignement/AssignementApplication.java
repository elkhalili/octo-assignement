package ma.octo.assignement;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@RequiredArgsConstructor
@SpringBootApplication

public class AssignementApplication {
	public static void main(String[] args) {
		SpringApplication.run(AssignementApplication.class, args);
	}
}
